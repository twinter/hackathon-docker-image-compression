FROM ubuntu:20.10

RUN apt-get update
RUN apt-get install -y python3 restic tar

RUN mkdir /input
RUN mkdir /output

COPY . /code

ENTRYPOINT ["python3", "/code/main.py"]
