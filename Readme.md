# Docker compression

This project was created in the Relaxdays Code Challenge Vol. 1. See https://sites.google.com/relaxdays.de/hackathon-relaxdays/startseite for more information. My participant ID in the challenge was: CC-VOL1-30

!!! This is a compression for docker images, not the file named `Dockerfile`. !!!

## built the image

with `docker build -t docker-compression .`

## running it

mount you docker image file into the container at `/input/dockerfile-to-compress` and a folder for the output `/output` when running the container, see below for examples

### compression

1. place the file in the projects root folder and name it `dockerfile-to-compress`
2. add `compress` to the `docker run` call. The complete call using the folders from above is `docker run -v "$(pwd)/dockerfile-to-compress:/input/dockerfile-to-compress" -v "$(pwd)/output:/output" -it docker-compression compress`

### decompression

1. place the file in the projects root folder and name it `dockerfile-to-compress.compressed`
2. add `decompress` to the `docker run` call. The complete call using the folders from above is `docker run -v "$(pwd)/dockerfile-to-compress.compressed:/input/dockerfile-to-compress.compressed" -v "$(pwd)/output:/output" -it docker-compression decompress`

### during development

- use this to mount the current folder: `docker run -v "$(pwd)/dockerfile-to-compress:/input/dockerfile-to-compress" -v "$(pwd)/output:/output" -v "$(pwd):/code" -it docker-compression compress`
