#!/usr/bin/env python3

import os
import sys
import logging
import shutil


INPUT_FILE_UNCOMPRESSED = '/input/dockerfile-to-compress'
INPUT_FILE_COMPRESSED = '/input/dockerfile-to-compress.compressed'
OUTPUT_FILE_COMPRESSED = '/output/dockerfile-to-compress.compressed'
OUTPUT_FILE_UNCOMPRESSED = '/output/dockerfile-to-compress'


uncompressed_image_path = '/tmp/uncompressed_image'
restic_repo_path = '/tmp/restic_repo'


def compress():
    assert os.path.isfile(INPUT_FILE_UNCOMPRESSED), 'input is not a file'
    logging.info('extracting archives')
    os.makedirs(uncompressed_image_path)
    os.system(f'tar -xf {INPUT_FILE_UNCOMPRESSED} -C {uncompressed_image_path}')
    for f in os.listdir(uncompressed_image_path):
        path = os.path.join(uncompressed_image_path, f)
        if os.path.isdir(path):
            layer_archive_path = os.path.join(path, 'layer.tar')
            layer_archive_uncompressed_path = os.path.join(path, "layer")
            os.makedirs(layer_archive_uncompressed_path)
            os.system(f'tar xf {layer_archive_path} -C {layer_archive_uncompressed_path}')
            os.remove(layer_archive_path)
    
    logging.info('building restic repository')
    os.system(f'echo "password" | restic init -r {restic_repo_path}')
    os.system(f'echo "password" | restic -r {restic_repo_path} --verbose backup {uncompressed_image_path}')
    
    logging.info('archive repository')
    os.system(f'tar -czf {OUTPUT_FILE_COMPRESSED} {restic_repo_path}')
    
    logging.info('removing temporary files')
    shutil.rmtree('/tmp/uncompressed_image')
    
    
def decompress():
    assert os.path.isfile(INPUT_FILE_COMPRESSED), 'input is not a file'
    logging.info('extracting archives')
    os.system(f'tar -xzf {INPUT_FILE_COMPRESSED}')
    
    logging.info('restoring from restic repo')
    os.makedirs(uncompressed_image_path)
    os.system(f'echo "password" | restic -r {restic_repo_path} restore latest --target /')
    
    for f in os.listdir(uncompressed_image_path):
        path = os.path.join(uncompressed_image_path, f)
        if os.path.isdir(path):
            layer_archive_uncompressed_path = os.path.join(path, "layer")
            layer_archive_path = os.path.join(path, 'layer.tar')
            os.system(f'tar cf {layer_archive_path} {layer_archive_uncompressed_path}')
            shutil.rmtree(layer_archive_uncompressed_path)

    logging.info('writing restored image file to output')
    os.system(f'tar -cf {OUTPUT_FILE_UNCOMPRESSED} {uncompressed_image_path}')


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout)
    
    if len(sys.argv) <= 1:
        logging.error('No argument provided! Call this with either "compress" or "decompress".')
    
    mode = sys.argv[1]
    if mode == 'compress':
        compress()
    elif mode == 'decompress':
        decompress()
    else:
        logging.error('Invalid argument provided! Call this with either "compress" or "decompress".')
